---
title: "About"
date: 2023-09-01T19:19:28-07:00
draft: false
rss_ignore: true
layout: page
menu:
    main:
        weight: 1
---

I'm John Nguyen, a senior at Sheldon High School. I am passionate about learning new skills, and I was super excited to combine my photography and web development skills to make this portfolio!
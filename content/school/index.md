---
title: "School"
date: 2023-09-01T19:35:17-07:00
draft: false
description: "\"Your first 10,000 photographs are your worst.\"  – Henri Cartier-Bresson"
featured_image: books_space.jpg
weight: 2
sort_by: Exif.Date
sort_order: desc
---

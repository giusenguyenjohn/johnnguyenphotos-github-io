---
title: "Sports"
date: 2023-09-15T17:11:54-07:00
draft: false
description: "\"The most important thing you learn as a sports photographer is anticipation - not where the action is taking place, but where it's going to take place. Not where the subject is now, but where they're going to be.\"  -Lawrence Schiller"
featured_image: two_boys_volleyball.jpg
weight: 4
sort_by: Exif.Date
sort_order: desc
---

